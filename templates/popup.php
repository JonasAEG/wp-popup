<?php
/*
 * Template for displaying the popup markup
 */

if ( ! defined( 'WPINC' ) ) {
	die( 'Direct access not allowed' );
}
?>

<div id="wp-popup-content" style="display: none !important;">
	<div id="wp-popup-inner">
		<?php echo $content; ?>
	</div>
</div>